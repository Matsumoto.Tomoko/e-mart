# README

ECサイト をローカルで起動する手順を記載します。

## with docker

`docker-ce` および `docker-compose` がインストールされている環境であれば、
ECサイト をカンタンに起動できます。

詳細なインストール手順は割愛します。
下記に詳細な手順が記載されていますので参照してインストールしてください。

* docker-ce ( https://docs.docker.com/glossary/?term=install )
* docker-compose ( https://docs.docker.com/compose/install/ )


## Install

Gitlab から clone し、下記コマンドを実行すると ECサイト の Docker イメージをビルドできます。

```
$ git clone git@xxxxxxxxxxxxx
$ cd container
$ docker-compose build
```

## Run

下記コマンドを実行すると ECサイト および連携するデータベースを起動できます。

```
$ docker-compose up -d
```

### Only for the first time

```
$ cd container/mysql
$ cp -pf codecamp21852.sql ./volumes/var/lib/mysql/.
$ cp -pf import.sh ./volumes/var/lib/mysql/.
$ docker exec -it mysql /var/lib/mysql/import.sh
```


## Access

http://localhost:8888/ec_top.php  
にアクセスすると ECサイトのログイン画面が表示されます。
