<?php
/***********************
* 管理側：ユーザー管理 *
************************/

require_once '../include/conf/const_basic.php';
require_once '../include/model/ec_common_model.php';
require_once '../include/model/ec_query_user_manage.php';

$user_list = array();

$url_root = dirname($_SERVER["REQUEST_URI"]).'/';

// セッション開始
session_start();

// セッション変数からログイン済みか確認
if(isset($_SESSION['user_id']) !== true){
    // ログイン済みでなかった場合、ログインページへリダイレクト
    header('Location:http://'. $_SERVER['HTTP_HOST'] . $url_root . 'ec_top.php');
    exit;
}

// DB接続
$link = get_db_connect();

// 商品一覧を取得
$user_list = get_user_list($link);

// 特殊文字をHTMLエンティティに変換
$user_list = entity_assoc_array($user_list);

// DB切断
close_db_connect($link);

include_once '../include/view/ec_user_manage_view.php';