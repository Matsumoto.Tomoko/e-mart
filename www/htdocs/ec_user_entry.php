<?php
/***************
* ユーザー登録 *
****************/

require_once '../include/conf/const_basic.php';
require_once '../include/model/ec_common_model.php';
require_once '../include/model/ec_query_user_entry.php';
require_once '../include/model/ec_error_check.php';

$request_method = "";
$user_name = '';
$passwd = '';
$user_id = '';

$err_msg = array();
$sql_error = array();
    
// セッション開始
session_start();

// データベース接続
$link = get_db_connect();
    
// リクエストメソッド確認
$request_method = get_request_method();

// リクエストメソッドをPOSTで受け取った場合
if($request_method === 'POST'){

    // POST値取得
    $user_name = get_post_data('user_name');
    $passwd    = get_post_data('passwd');
    
    // エラーチェック（ユーザー名とパスワードの値）
    $has_no_error = user_and_passwd_check($user_name, $passwd);
    if($has_no_error !== true){
        $err_msg[] = $has_no_error;
    }
    
    // エラーがなかった場合
    if(count($err_msg) === 0){
        // エラーチェック（同一ユーザー名）
        $has_no_error = same_user_check($link, $user_name);
        
        if($has_no_error !== true){
            $err_msg[] = $has_no_error;
        // ユーザーtableにしユーザー情報を追加
        }else if(insert_new_user($link, $user_name, $passwd) === true){
            // A_Iを取得
            $user_id = mysqli_insert_id($link);
            $msg = 'ユーザーを追加しました';
        }else{
            $sql_error[] = 'ユーザーの追加に失敗しました';
        }
    }
    
    // DB切断
    close_db_connect($link);
}

include_once '../include/view/ec_user_entry_view.php';