<?php
/*********
* カート *
**********/

require_once '../include/conf/const_basic.php';
require_once '../include/model/ec_common_model.php';
require_once '../include/model/ec_error_check.php';
require_once '../include/model/ec_query_cart_and_result.php';

$user_id = null;
$request_method = '';
$item_id = null;
$amount = null;

$msg = '';
$err_msg = array();
$sql_error = array();

$cart_list = array();
$total_amount = null;

$url_root = dirname($_SERVER["REQUEST_URI"]).'/';

// セッション開始
session_start();
// セッション変数からログイン済みか確認
if(isset($_SESSION['user_id']) !== true){
    // ログイン済みでなかった場合、ログインページへリダイレクト
    header('Location:http://'. $_SERVER['HTTP_HOST'] . $url_root . 'ec_top.php');
    exit;
}else{
    $user_id = $_SESSION['user_id'];
}

// DB接続
$link = get_db_connect();

// リクエストメソッドの取得
$request_method = get_request_method();

// リクエストメソッドをPOSTで受け取った場合
if($request_method === 'POST'){
    // POSTデータを取得
    $sql_kind = get_post_data('sql_kind');
    
    // 商品削除の場合
    if($sql_kind === 'delete'){
        // POSTデータを取得
        $item_id = get_post_data('item_id');
        
        // 指定商品の削除
        if(delete_item_from_cart_table($link, $user_id, $item_id) === true){
            $msg = '商品をカートから削除しました';
        }else{
            $sql_error[] = '商品の削除に失敗しました';
        }
        
    // 数量変更の場合
    }else if($sql_kind === 'update_amount'){
        // POSTデータを取得
        $item_id = get_post_data('item_id');
        $amount  = get_post_data('amount');
        
        // エラーチェック
        $has_no_error = ammount_check($amount);
        if($has_no_error !== true){
            $err_msg[] = $has_no_error;
        }
        
        // エラーがない場合、数量の変更
        if(count($err_msg) === 0){
            if(update_buy_amount($link, $user_id, $item_id, $amount) === true){
                $msg = '数量を変更しました';
            }else{
                $sql_error[] = '数量の変更に失敗しました';
            }
        }
    }
}

// カート情報を取得
$cart_list = get_cart_list($link, $user_id);

// 特殊文字をHTMLエンティティに変換
$cart_list = entity_assoc_array($cart_list);

// カート内の合計金額を取得
$total_amount = calculate_total_price($cart_list);

// DB切断
close_db_connect($link);

include_once '../include/view/ec_cart_view.php';