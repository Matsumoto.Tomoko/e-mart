<?php
/*******************
* 管理側：商品管理 *
********************/


require_once '../include/conf/const_basic.php';
require_once '../include/model/ec_common_model.php';
require_once '../include/model/ec_error_check.php';
require_once '../include/model/ec_query_tool.php';

$request_method = '';
$user_name = '';
$passwd = '';

$new_name = '';
$new_price = null;
$new_stock = null;
$new_image_name = '';
$new_status = '';

$item_id = null;
$stock_id = null;
$update_stock = null;

$item_list = array();

$msg = '';
$err_msg = array();
$sql_error = array();

$url_root = dirname($_SERVER["REQUEST_URI"]).'/';

// セッション開始
session_start();
// セッション変数からログイン済みか確認
if(isset($_SESSION['user_id']) !== true){
    // ログイン済みでなかった場合、ログインページへリダイレクト
    header('Location:http://'. $_SERVER['HTTP_HOST'] . $url_root . 'ec_top.php');
    exit;
}

// DB接続
$link = get_db_connect();

// リクエストメソッドの取得
$request_method = get_request_method();

// リクエストメソッドをPOSTで受け取った場合
if($request_method === 'POST'){
    //POSTデータを取得
    $sql_kind = get_post_data('sql_kind');
    
    // 商品追加の場合
    if($sql_kind === 'insert'){
        //POSTデータを取得
        $new_name   = get_post_data('new_name');
        $new_price  = get_post_data('new_price');
        $new_stock  = get_post_data('new_stock');
        $new_status = get_post_data('new_status');
        
        // ファイルのアップロード
        if(is_uploaded_file($_FILES['new_img']['tmp_name'])){
            $sha1 = sha1(uniqid(mt_rand(), true));
            //getimagesize関数で画像情報を取得する
            $image_info = getimagesize($_FILES['new_img']['tmp_name']);
            
            if($image_info === false){
                $err_msg[] = '画像情報を取得できませんでした';
            }else{
                
                //list関数の第3引数にはgetimagesize関数で取得した画像のMIMEタイプが格納されているので条件分岐で拡張子を決定する
                switch($image_info[2]){
                    //jpegの場合
                    case IMAGETYPE_JPEG:
                        //拡張子の設定
                        $ext = 'jpg';
                        break;
                    //pngの場合
                    case IMAGETYPE_PNG:
                        //拡張子の設定
                        $ext = 'png';
                        break;
                    default:
                        $ext = '';
                        break;
                }
                // エラーチェック（ファイルアップロード）
                if($ext === ''){
                    $err_msg[] = 'アップロードする画像のファイル形式は「JPEG」「PNG」のみ可能です';
                }
                $new_image_name = $sha1 . '.' .$ext;
                if(move_uploaded_file($_FILES['new_img']['tmp_name'], './images/' . $new_image_name) !== true){
                    $err_msg[] = 'アップロードに失敗しました';
                }
            }
        }else{
                $err_msg[] = '商品画像を選択してください';
        }
            
        
        // エラーチェック
        $has_no_error = item_name_check($new_name);
        if($has_no_error !== true){
            $err_msg[] = $has_no_error;
        }
        
        $has_no_error = price_check($new_price);
        if($has_no_error !== true){
            $err_msg[] = $has_no_error;
        }
        
        $has_no_error = stock_check($new_stock);
        if($has_no_error !== true){
            $err_msg[] = $has_no_error;
        }
        
        $has_no_error = status_check($new_status);
        if($has_no_error !== true){
            $err_msg[] = $has_no_error;
        }
        
        // エラーがなかった場合
        if(count($err_msg) === 0){
            // 更新系の処理を行う前にトランザクション開始(オートコミットをオフ）
            transaction_start($link);
            
            // 新商品情報を商品tableにinsert
            if(insert_new_item_into_item_table($link, $new_name, $new_price, $new_image_name, $new_status) === true){
                // A_Iを取得
                $item_id = mysqli_insert_id($link);
                
                // 新商品情報を在庫管理tableにinsert
                if(insert_new_item_ec_stock_table($link, $item_id, $new_stock) === true){
                    // A_Iを取得
                    $stock_id = mysqli_insert_id($link);
                    
                }else{
                    $sql_error[] = '商品の追加に失敗しました';
                }
            }else{
                $sql_error[] = '商品の追加に失敗しました';
            }
            
            // トランザクション成否判定
            if(transaction_commit_or_rollback($link, $sql_error) === true){
                $msg = '商品を追加しました';
            }
        }
        
    // 在庫数変更の場合
    }else if($sql_kind === 'update_stock_num'){
        // POSTデータを取得
        $item_id      = get_post_data('item_id');
        $update_stock = get_post_data('update_stock');
        
        // エラーチェック
        $has_no_error = stock_check($update_stock);
        if($has_no_error !== true){
            $err_msg[] = $has_no_error;
        }
        
        // エラーがなかった場合
        if(count($err_msg) === 0){
            // 在庫管理tableをupdate
            if(update_stock_number($link, $update_stock, $item_id) !== true){
                $sql_error[] = '在庫数の変更に失敗しました';
            }else{
                $msg = '在庫数を変更しました';
            }
        }
        
    // ステータス変更の場合
    }else if($sql_kind === 'update_status'){
        // POSTデータを取得
        $item_id = get_post_data('item_id');
        
        // 商品tableをupdate
       if(update_status($link, $item_id) !== true){
            $sql_error[] = 'ステータスの変更に失敗しました';
        }else{
            $msg = 'ステータスを変更しました';
        }
        
    // 指定商品の削除の場合
    }else if($sql_kind === 'delete'){
        // POSTデータを取得
        $item_id = get_post_data('item_id');
        
        // 更新系の処理を行う前にトランザクション開始(オートコミットをオフ）
        transaction_start($link);
        
        // 商品tableからdelete
        if(delete_from_ec_item_table($link, $item_id) === true){
            // 在庫管理tableからdelete
            if(delete_from_ec_stock_table($link, $item_id) !== true){
                $sql_error[] = '商品の削除に失敗しました';
            }
        }else{
            $sql_error[] = '商品の削除に失敗しました';
        }
        
        // トランザクション成否判定
        if(transaction_commit_or_rollback($link, $sql_error) === true){
            $msg = '商品を削除しました';
        }
    }
}

// 商品一覧を取得
$item_list = get_item_list_for_manage($link);

// 特殊文字をHTMLエンティティに変換
$item_list = entity_assoc_array($item_list);

// DB切断
close_db_connect($link);

include_once '../include/view/ec_tool_view.php';