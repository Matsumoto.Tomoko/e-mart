<?php
/*******************
* 管理側：商品管理 *
********************/

require_once '../include/conf/const_basic.php';
require_once '../include/model/ec_common_model.php';
require_once '../include/model/ec_error_check.php';
require_once '../include/model/ec_query_cart_and_result.php';

$user_id = null;
$request_method = '';
$cart_list = array();
$total_amount = null;
$item_id = null;
$amount = null;
$sql_error = array();
$err_msg = array();
$msg = '';

$url_root = dirname($_SERVER["REQUEST_URI"]).'/';

// セッション開始
session_start();
// セッション変数からログイン済みか確認
if(isset($_SESSION['user_id']) !== true){
    // ログイン済みでなかった場合、ログインページへリダイレクト
    header('Location:http://'. $_SERVER['HTTP_HOST'] . $url_root . 'ec_top.php');
    exit;
}else{
    $user_id = $_SESSION['user_id'];
}

// DB接続
$link = get_db_connect();

// リクエストメソッドの取得
$request_method = get_request_method();

// リクエストメソッドをPOSTで受け取った場合
if($request_method === 'POST'){
    // ユーザーのカート情報を取得(fileは"ec_query_cart.php")
    $cart_list = get_cart_list($link, $user_id);
    
    // 特殊文字をHTMLエンティティに変換
    $cart_list = entity_assoc_array($cart_list);
    
    // カート内の合計金額を取得(fileは"ec_query_cart.php")
    $total_amount = calculate_total_price($cart_list);
    
    // エラーチェック（商品未選択の場合）
    if($total_amount === 0){
        $err_msg[] = '商品を選択してください';
    }
    
    // エラーチェック（購入ボタン押下時の在庫数とステータス）
    $has_no_error = now_status_check($cart_list);
    if($has_no_error !== true){
        $err_msg[] = $has_no_error;
    }else{
        $has_no_error = now_stock_check($cart_list);
        if($has_no_error !== true){
            $err_msg[] = $has_no_error;
        }
    }
    
    if(count($err_msg) === 0){
        // トランザクション開始
        transaction_start($link);
        
        // foreach文でrow毎にDBの編集
        foreach($cart_list as $key){
            $item_id = $key['item_id'];
            $amount  = $key['amount'];
            
            // 購入商品の在庫tableの在庫数を減らす
            if(decrement_stock_number($link, $amount, $item_id) === true){
                // カートから対象の商品情報を削除する
                if(delete_item_from_cart_table($link, $user_id, $item_id) !== true){
                    $sql_error[] = 'カートの削除に失敗しました';
                }
            }else{
                $sql_error[] = '在庫数の変更に失敗しました';
            }
        }
        
        // トランザクション成否判定
        if(transaction_commit_or_rollback($link, $sql_error) === true){
            $msg = '購入ありがとうございました！';
        }
    }
}

// DB切断
close_db_connect($link);

include_once '../include/view/ec_result_view.php';