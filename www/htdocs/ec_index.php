<?php
/***********
* 商品一覧 *
************/

require_once '../include/conf/const_basic.php';
require_once '../include/model/ec_common_model.php';
require_once '../include/model/ec_query_index.php';

$request_method = '';
$user_id = null;
$item_id = null;
$cart_id = null;

$msg = '';
$sql_error = array();

$item_list = array();

// セッション開始
session_start();

// セッション変数からログイン済みか確認
if(isset($_SESSION['user_id']) !== true){
    // ログイン済みでなかった場合、ログインページへリダイレクト
    $url_root = dirname($_SERVER["REQUEST_URI"]).'/';
    header('Location: http://'. $_SERVER['HTTP_HOST'] . $url_root . 'ec_top.php');
    exit;
}else{
    $user_id = $_SESSION['user_id'];
}

// DB接続
$link = get_db_connect();

// リクエストメソッドの取得
$request_method = get_request_method();

if($request_method === 'POST'){
    //POSTデータを取得
    $item_id = get_post_data('item_id');
    
    // 指定した商品がカートへ追加済みか調べる
    $data = array();
    $data = search_same_item($link, $user_id, $item_id);
    
    // 指定していなかった場合はカートtableに情報を追加する
    if(count($data) === 0){
        if(insert_cart_table($link, $user_id, $item_id) !== true){
            $sql_error[] = 'カートへの追加に失敗しました';
        }else{
            // A_Iを取得
            $cart_id = mysqli_insert_id($link);
            $msg = 'カートへ追加しました';
        }
    // 指定済みの場合は数量を+1にする
    }else{
        $cart_id = $data[0]['cart_id'];
        
        if(add_amount_cart_table($link, $cart_id) !== true){
            $sql_error[] = '数量の追加に失敗しました';
        }else{
            $msg = '数量を追加しました';
        }
    }
}

// 公開中の商品一覧を取得
$item_list = get_item_list_for_buy($link);

// 特殊文字をHTMLエンティティに変換
$item_list = entity_assoc_array($item_list);

// DB切断
close_db_connect($link);

include_once '../include/view/ec_index_view.php';