<?php
/*************** 
* ログイン処理 *
****************/
require_once '../include/conf/const_basic.php';
require_once '../include/model/ec_common_model.php';
require_once '../include/model/ec_query_login.php';

$request_method = "";
$user_name = '';
$passwd = '';
$data = array();

$url_root = dirname($_SERVER["REQUEST_URI"]).'/';

// リクエストメソッド確認
$request_method = get_request_method();
if($request_method === 'POST'){
    
    // セッション開始
    session_start();
    
    // POST値取得
    $user_name = get_post_data('user_name');
    $passwd    = get_post_data('passwd');
    
    // ユーザー名をCookieへ保存
    setcookie('user_name', $user_name, time() + 60 * 60* 24 * 365);
    
    // 管理用ユーザ（ID:admin ,パスワード:admin）でログインした場合
    if($user_name === 'admin' && $passwd === 'admin'){
        // セッション変数にuser_idを保存(管理者のユーザーIDは「０」とする)
        $_SESSION['user_id'] = 0;
        // 「管理側の商品管理ページ」に遷移
        header('Location: http://'. $_SERVER['HTTP_HOST'] . $url_root . 'ec_tool.php');
        exit;
    }
    
    // データベース接続
    $link = get_db_connect();
    
    // ユーザー名とパスワードからユーザーIDを取得
    $data = get_user_id($link, $user_name, $passwd);
    
    // データベース切断
    close_db_connect($link);
    
    // 登録データを取得できたか確認
    if(isset($data[0]['user_id']) === true){
        // セッション変数にuser_idを保存
        $_SESSION['user_id'] = $data[0]['user_id'];
        // 「商品一覧ページ」に遷移
        header('Location: http://'. $_SERVER['HTTP_HOST'] . $url_root . 'ec_index.php');
        exit;
    }else{
        // セッション変数にログインのエラーフラグを保存
        $_SESSION['login_err_flag'] = TRUE;
        // 「ログイン」に遷移
        header('Location: http://'. $_SERVER['HTTP_HOST'] . $url_root . 'ec_top.php');
        exit;
    }
}