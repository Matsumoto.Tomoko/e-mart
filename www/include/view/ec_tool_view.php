<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <title>e-mart</title>
        <!--<link rel="stylesheet" href="./css/ec.css">-->
        <link href="./css/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <header>
            <div class="container-fluid bg-info text-white p-4">
                <img src="./images/logo.png" class="logo">
                <div class="float-right">
                    <a href="./ec_user_manage.php" class="btn btn-info">ユーザー管理ページへ</a>
                    <a href="./ec_logout.php" class="btn btn-info">ログアウト</a>
                </div>
                <h2 class="ml-3 mt-3 mr-3">管理ページ</h2>
            </div>
        </header>
        <div class="container">
<!-- メッセージ表示 -->
<?php foreach ($err_msg as $read) { ?>
            <p class="m-3"><h4 class="text-danger"><?php print $read; ?></h4></p>
<?php } ?>
<?php foreach ($sql_error as $read) { ?>
            <p class="m-3"><h4 class="text-danger"><?php print $read; ?></h4></p>
<?php } ?>
<?php if(isset($msg) === true){ ?>
            <p class="m-3"><h4 class="text-danger"><?php print $msg; ?></h4></p>
<?php } ?>
            <section>
                <h2>商品の登録</h2>
                <form method="post" action="ec_tool.php" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="new_name">商品名　</label>
                        <input type="text" id="new_name" name="new_name" value="" class="form-control-sm">
                    </div>
                    <div class="form-group">
                        <label for="new_price">値段　　</label>
                        <input type="text" id="new_price" name="new_price" value="" class="form-control-sm">
                    </div>
                    <div class="form-group">
                        <label for="new_stock">在庫数　</label>
                        <input type="text" id="new_stock" name="new_stock" value="" class="form-control-sm">
                    </div>
                    <div class="form-group">
                        <label for="new_img">商品画像</label>
                        <input type="file" id="new_img" name="new_img" class="form-control-sm">
                    </div>
                    <div class="form-group">
                        <select name="new_status" class="custom-select custom-select-sm" style="width: 250px">
                            <option value="0">非公開</option>
                            <option value="1">公開</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="商品を登録する" class="btn btn-info mb-3">
                        <input type="hidden" name="sql_kind" value="insert">
                    </div>
                </form>
            </section>
            <section>
                <h2>商品情報の一覧・変更</h2>
                <table  class="table table-hover">
                    <tr class="table-primary">
                        <th>商品画像</th>
                        <th>商品名</th>
                        <th>価格</th>
                        <th>在庫数</th>
                        <th>ステータス</th>
                        <th>操作</th>
                    </tr>
<!-- foreachでtable作成 -->
<?php foreach ($item_list as $item) { ?>
<!-- ステータスの切り替え -->
<?php if ($item['status'] === '0') { ?>
                    <tr class="table-dark">
<?php }else{ ?>
                    <tr>
<?php } ?>
                        <td><img src="images/<?php print $item['img'] ?>"></td>
                        <td class="item_name_width"><?php print $item['name']; ?></td>
                        <td class="text_align_right"><?php print $item['price']; ?>円</td>
                        <td>
                            <form method="post">
                                <input type="text" class="input_text_width text_align_right" name="update_stock" value="<?php print $item['stock'] ;?>">
                                <input type="submit" value="変更" class="btn btn-info">
                                <input type="hidden" name="item_id" value="<?php print $item['item_id'] ;?>">
                                <input type="hidden" name="sql_kind" value="update_stock_num">
                            </form>
                        </td>
                        <td>
                            <form method="post">
                                <input type="submit" value="ステータス変更" class="btn btn-info">
                                <input type="hidden" name="item_id" value="<?php print $item['item_id'] ;?>">
                                <input type="hidden" name="sql_kind" value="update_status">
                            </form>
                        </td>
                        <td>
                            <form method="post">
                                <input type="submit" value="削除" class="btn btn-info">
                                <input type="hidden" name="item_id" value="<?php print $item['item_id'] ;?>">
                                <input type="hidden" name="sql_kind" value="delete">
                            </form>
                        </td>
                    </tr>
<?php } ?>
                </table>
            </section>
        </div>
        <script src="./css/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>