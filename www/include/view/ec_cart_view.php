<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <title>e-mart</title>
        <link href="./css/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <header>
            <div class="container-fluid bg-info text-white p-4">
                <img src="./images/logo.png" class="logo">
                <div class="float-right">
                    <a href="./ec_index.php" class="btn btn-info">商品一覧へ</a>
                    <a href="./ec_logout.php" class="btn btn-info">ログアウト</a>
                </div>
            </div>
        </header>
        <div class="container">
    <!-- メッセージ表示 -->
<?php foreach ($err_msg as $read) { ?>
            <p class="m-3"><h4 class="text-danger"><?php print $read; ?></h4></p>
<?php } ?>
<?php foreach ($sql_error as $read) { ?>
            <p class="m-3"><h4 class="text-danger"><?php print $read; ?></h4></p>
<?php } ?>
<?php if(isset($msg) === true){ ?>
            <p class="m-3"><h4 class="text-danger"><?php print $msg; ?></h4></p>
<?php } ?>
            <h2>ショッピングカート</h2>
            <table class="table table-hover">
                <tr class="table-primary">
                    <th>画像</th>
                    <th>商品名</th>
                    <th>値段</th>
                    <th>数量</th>
                </tr>
<?php foreach($cart_list as $cart){ ?>
                <tr>
                    <td><img src="images/<?php print $cart['img']; ?>"></td>
                    <td>
                        <form method="post">
                            <?php print $cart['name']; ?>
                            <input class="btn btn-info" type="submit" value="削除">
                            <input type="hidden" name="sql_kind" value="delete">
                            <input type="hidden" name="item_id" value="<?php print $cart['item_id']; ?>">
                        </form>
                    </td>
                    <td><?php print $cart['price']; ?></td>
                    <td>
                        <form method="post">
                            <input type="text" class="input_text_width text_align_right" name="amount" value="<?php print $cart['amount']; ?>">個
                            <input class="btn btn-info" type="submit" value="変更する">
                            <input type="hidden" name="sql_kind" value="update_amount">
                            <input type="hidden" name="item_id" value="<?php print $cart['item_id']; ?>">
                        </form>
                    </td>
                </tr>
<?php } ?>
            </table>
            <div class="mx-auto" style="width: 250px">
                <h2>合計金額：<?php print $total_amount; ?>円</h2>
            </div>
            <div class="mx-auto m-3" style="width: 110px">
                <form method="post" action="./ec_result.php">
                    <input class="btn btn-info btn-lg" type="submit" value="  購入する  ">
                </form>
            </div>
        </div>
        <script src="./css/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>