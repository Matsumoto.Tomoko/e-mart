<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <title>e-mart</title>
        <link href="./css/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <header>
            <div class="container-fluid bg-info text-white p-4">
                <img src="./images/logo.png" class="logo">
                <div class="float-right">
                    <a href="./ec_index.php" class="btn btn-info">商品一覧へ</a>
                    <a href="./ec_logout.php" class="btn btn-info">ログアウト</a>
                </div>
            </div>
        </header>
        <div class="container">
<!-- メッセージ表示 -->
<?php foreach ($err_msg as $read) { ?>
            <p class="m-3"><h4 class="text-danger"><?php print $read; ?></h4></p>
<?php } ?>
<?php foreach ($sql_error as $read) { ?>
            <p class="m-3"><h4 class="text-danger"><?php print $read; ?></h4></p>
<?php } ?>
<?php if(isset($msg) === true){ ?>
            <p class="m-3"><h4 class="text-info"><?php print $msg; ?></h4></p>
<?php } ?>
            <table class="table">
                <tr class="table-primary">
                    <th>画像</th>
                    <th>商品名</th>
                    <th>値段</th>
                    <th>数量</th>
                </tr>
<?php foreach($cart_list as $cart){ ?>
                <tr>
                    <td><img src="images/<?php print $cart['img']; ?>"></td>
                    <td><?php print $cart['name']; ?></td>
                    <td><?php print $cart['price']; ?></td>
                    <td><?php print $cart['amount']; ?>個</td>
                </tr>
<?php } ?>
            </table>
            <div class="mx-auto" style="width: 250px">
                <h2>合計金額：<?php print $total_amount; ?>円</h2>
            </div>
        </div>
        <script src="./css/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>