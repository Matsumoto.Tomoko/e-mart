<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <title>e-mart</title>
        <!--<link rel="stylesheet" href="./css/ec.css">-->
        <link href="./css/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <header>
            <div class="container-fluid bg-info text-white p-4">
                <img src="./images/logo.png" class="logo">
                <div class="float-right">
                    <a href="./ec_tool.php" class="btn btn-info">商品管理ページへ</a>
                    <a href="./ec_logout.php" class="btn btn-info">ログアウト</a>
                </div>
                <h2 class="ml-3 mt-3 mr-3">管理ページ</h2>
            </div>
        </header>
        <div class="container p-5">
            <h2>ユーザ情報一覧</h2>
            <table class="table">
                <tr class="table-primary">
                    <th>ユーザID</th>
                    <th>登録日時</th>
                </tr>
<?php foreach($user_list as $user){ ?>
                <tr>
                    <td><?php print $user['user_name']; ?></td>
                    <td><?php print $user['created_date']; ?></td>
                </tr>
<?php } ?>
            </table>
        </div>
        <script src="./css/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>