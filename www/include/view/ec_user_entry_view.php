<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <title>e-mart</title>
        <link href="./css/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <header>
            <div class="container-fluid bg-info text-white p-4">
                <img src="./images/logo.png" class="logo">
            </div>
        </header>
        <div class="container">
<!-- メッセージ表示 -->
<?php foreach ($err_msg as $read) { ?>
            <p class="m-3"><h4 class="text-danger"><?php print $read; ?></h4></p>
<?php } ?>
<?php foreach ($sql_error as $read) { ?>
            <p class="m-3"><h4 class="text-danger"><?php print $read; ?></h4></p>
<?php } ?>
<?php if(isset($msg) !== true){ ?>
            <p class="m-3"><h4 class="text-info"><?php print $msg; ?></h4></p>
<?php } ?>
            <div class="m-5">
                <form method="post" action="ec_user_entry.php">
                    <div class="form-group">
                        <input type="text" name="user_name" value="" placeholder="ユーザー名" class="form-control-sm m-2">
                    </div>
                    <div class="form-group">
                        <input type="password" name="passwd" value="" placeholder="パスワード" class="form-control-sm m-2">
                    </div>
                    <div class="form-group">
                        <input type="submit" value="ユーザーを新規登録する" class="btn btn-info m-2">
                    </div>
                </form>
                <a href="./ec_top.php">ログインページに移動する</a>
            </div>
        </div>
        <script src="./css/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>