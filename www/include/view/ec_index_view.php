<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <title>e-mart</title>
        <link href="./css/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <header>
            <div class="container-fluid bg-info text-white p-4">
                <img src="./images/logo.png" class="logo">
                <div class="float-right">
                    <a href="./ec_cart.php" class="btn btn-info">カート</a>
                    <a href="./ec_logout.php" class="btn btn-info">ログアウト</a>
                </div>
            </div>
        </header>
        <div class="container">
<!-- メッセージ表示 -->
<?php foreach ($sql_error as $read) { ?>
            <p class="m-3"><h4 class="text-danger"><?php print $read; ?></h4></p>
<?php } ?>
<?php if(isset($msg) === true){ ?>
            <p class="m-3"><h4 class="text-danger"><?php print $msg; ?></h4></p>
<?php } ?>
            <div class="row align-items-center">
<?php foreach($item_list as $item){ ?>
                <div class="d-flex justify-content-around">
                    <ul class="list-unstyled text-center m-3">
                        <li><img src="images/<?php print $item['img']; ?>"></li>
                        <li><?php print $item['name']; ?></li>
                        <li><?php print $item['price']; ?>円</li>
<?php if($item['stock'] !== '0'){ ?>
                        <li>
                            <form method="post" action="./ec_index.php">
                                <input type="submit" class="btn btn-info" value="カートに入れる">
                                <input type="hidden" name="item_id" value="<?php print $item['item_id']; ?>">
                            </form>
                        </li>
<?php }else{ ?>
                        <li class="text-danger">売り切れ</li>
<?php } ?>
                    </ul>
                </div>
<?php } ?>
            </div>
        </div>
        <script src="./jquery/jquery-3.3.1.min.js"></script>
        <script src="./css/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>