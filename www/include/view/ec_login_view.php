<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <title>e-mart</title>
        <link href="./css/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <header>
            <div class="container-fluid bg-info text-white p-4">
                <img src="./images/logo.png" class="logo">
            </div>
        </header>
        <div class="container">
<?php if($login_err_flag === true){ ?>
            <p class="m-3"><h4 class="text-danger">「ユーザー名」または「パスワード」が違います</h4></p>
<?php } ?>
            <div class="m-5">
                <form method="post" action="ec_login.php">
                    <div class="form-group">
                        <input type="text" name="user_name" value="<?php print $user_name ?>" placeholder="ユーザー名" class="form-control-sm m-2">
                    </div>
                    <div class="form-group">
                        <input type="password" name="passwd" value="" placeholder="パスワード" class="form-control-sm m-2">
                    </div>
                    <div class="form-group">
                        <input type="submit" value="ログイン" class="btn btn-info m-2">
                    </div>
                </form>
                <a href="./ec_user_entry.php">ユーザーの新規作成はこちら</a>
            </div>
        </div>
        <script src="./css/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>