<?php

/*************
* user_manage *
**************/

/**
* ユーザーリストの取得
* @param obj $link DBハンドル
* @return array ユーザー配列データ
*/
function get_user_list($link){
    $sql = 'SELECT user_name, created_date FROM ec_user_table';
    
    return get_as_array($link, $sql);
}