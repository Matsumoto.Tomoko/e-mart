<?php
/***************
* cart & result*
***************/
/**
* カート情報の取得
* @param obj $link DBハンドル
* @param $user_id
* @return array カート配列データ
*/
function get_cart_list($link, $user_id){
    $sql = 'SELECT ct.cart_id, ct.item_id, ct.amount, it.img, it.name, it.price, it.status, st.stock 
            FROM ec_cart_table AS ct 
            LEFT JOIN ec_item_table AS it ON ct.item_id = it.item_id 
            LEFT JOIN ec_stock_table AS st ON it.item_id = st.item_id 
            WHERE user_id = ' . $user_id;
    
    return get_as_array($link, $sql);
}

/**
* カートの対象商品の数量変更
* @param obj $link DBハンドル
* @param $user_id $item_id $amount
* @return bool
*/
function update_buy_amount($link, $user_id, $item_id, $amount){
    $sql = 'UPDATE ec_cart_table SET amount = ' . $amount . ' , updated_date = now() WHERE user_id = ' . $user_id . ' AND item_id = ' . $item_id;
    
    return edit_db($link, $sql);
}

/**
* カートから対象の商品情報を削除
* @param obj $link DBハンドル
* @param $user_id $item_id
* @return bool
*/
function delete_item_from_cart_table($link, $user_id, $item_id){
    $sql = 'DELETE FROM ec_cart_table WHERE user_id = ' . $user_id . ' AND item_id = ' .$item_id;
    
    return edit_db($link, $sql);
}

/**
* カート内商品の合計金額の計算
* @param array カート配列データ
* @return 
*/
function calculate_total_price($array_data){
    $num = 0;
    
    foreach($array_data as $value){
        $num += $value['price'] * $value['amount'] ;
    }
    
    return $num;
}

/**
* 購入商品の在庫tableの在庫数を減らす
* @param obj $link DBハンドル
* @param $item_id　$amount
* @return bool
*/
function decrement_stock_number($link, $amount, $item_id){
    $sql = 'UPDATE ec_stock_table SET stock = stock - ' . $amount . ' , updated_date = now() WHERE item_id = ' . $item_id;
    
    return edit_db($link, $sql);
}
