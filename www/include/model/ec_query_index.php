<?php

/********
* index *
*********/

/**
* カートIDの取得
* @param obj $link DBハンドル
* @param $user_id、$item_id
* @return array ユーザー配列データ
*/
function search_same_item($link, $user_id, $item_id){
    $sql = 'SELECT cart_id FROM ec_cart_table WHERE user_id = ' .$user_id. ' AND item_id = ' .$item_id;
    
    return get_as_array($link, $sql);
}

/**
* カートtableへの追加
* @param obj $link DBハンドル
* @param $user_id、$item_id
* @return bool
*/
function insert_cart_table($link, $user_id, $item_id){
    $sql = 'INSERT INTO ec_cart_table(user_id, item_id, amount, created_date, updated_date) 
            VALUES(' . $user_id . ',' . $item_id . ', 1, now(), now())';
    
    return edit_db($link, $sql);
}

/**
* カートtableへ数量の追加（すでに同一商品が登録されていた場合）
* @param obj $link DBハンドル
* @param $cart_id
* @return bool
*/
function add_amount_cart_table($link, $cart_id){
    $sql = 'UPDATE ec_cart_table SET amount = amount + 1, updated_date = now() WHERE cart_id = ' . $cart_id;
    
    return edit_db($link, $sql);
}

/**
* 商品情報の取得（ステータスが1の商品のみ）
* @param obj $link DBハンドル
* @return array 商品配列データ
*/
function get_item_list_for_buy($link){
    $sql = 'SELECT it.item_id, it.name, it.price, it.img, st.stock FROM ec_item_table AS it
            LEFT JOIN ec_stock_table AS st ON st.item_id = it.item_id WHERE it.status = 1;';
    
    return get_as_array($link, $sql);
}
