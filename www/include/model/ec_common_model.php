<?php
/**
* リクエストメソッドを取得
* @return str GET/POST/PUTなど
**/
function get_request_method() {
   return $_SERVER['REQUEST_METHOD'];
}

/**
* POSTデータを取得
* @param str $key 配列キー
* @return str POST値
*/
function get_post_data($key){
    $str = '';
    if(isset($_POST[$key]) === true){
        $str = trim(mb_convert_kana($_POST[$key], 'ns', HTML_CHARACTER_SET));
    }
    return $str;
}

/**
* DBハンドルを取得
* @return obj $link DBハンドル
*/
function get_db_connect(){
    if(!$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWD, DB_NAME)){
        die("DB接続に失敗しました");
    }
    // 文字コードセット
    mysqli_set_charset($link, DB_CHARACTER_SET);
    
    return $link;
}

/**
* DBとのコネクション切断
* @param obj $link DBハンドル
*/
function close_db_connect($link){
    mysqli_close($link);
}

/**
* クエリを実行する（insert or update）
*
* @param obj $link DBハンドル
* @param str SQL文
* @return bool
*/
function edit_db($link, $sql) {
   // クエリを実行する
   if (mysqli_query($link, $sql) === true) {
       return true;
   } else {
       return false;
   }
}

/**
* クエリを実行しその結果を配列で取得する(select)
*
* @param obj  $link DBハンドル
* @param str  $sql SQL文
* @return array 結果配列データ
*/
function get_as_array($link, $sql){
    
    // 返却用配列
    $data = array();
    
    if($result = mysqli_query($link, $sql)){
        if(mysqli_num_rows($result) > 0){
            while($row = mysqli_fetch_assoc($result)){
                $data[] = $row;
            }
        }
        // 結果セットを開放
        mysqli_free_result($result);
    }
    return $data;
}

/**
* トランザクション開始(オートコミットをオフ）
*/
function transaction_start($link){
    mysqli_autocommit($link, false);
}

/**
* トランザクション成否判定
* @return bool
*/
function transaction_commit_or_rollback($link, $error_array){
    if (count($error_array) === 0) {
        // 処理確定
        mysqli_commit($link);
        return true;
    }else{
        // 処理取消
        mysqli_rollback($link);
        return false;
    }
}

/**
* 特殊文字をHTMLエンティティに変換する
* @param str  $str 変換前文字
* @return str 変換後文字
*/
function entity_str($str){
    return htmlspecialchars($str, ENT_QUOTES, HTML_CHARACTER_SET);
}

/**
* 特殊文字をHTMLエンティティに変換する(2次元配列の値)
* @param array  $assoc_array 変換前配列
* @return array 変換後配列
*/
function entity_assoc_array($assoc_array) {
    foreach ($assoc_array as $key => $value) {
        foreach ($value as $keys => $values) {
            // 特殊文字をHTMLエンティティに変換
            $assoc_array[$key][$keys] = entity_str($values);
        }
    }
    return $assoc_array;
}
