<?php

/*************
* user_entry *
**************/

/**
* ユーザーリストの取得
* @param obj $link DBハンドル
* @param $user_name
* @return array ユーザー配列データ
*/
function get_user_name($link, $user_name){
    $sql = 'SELECT user_name FROM ec_user_table WHERE user_name = \'' .$user_name. '\'';
    
    return get_as_array($link, $sql);
}

/**
* 新規ユーザーの登録
* @param obj $link DBハンドル
* @param $user_name、$passwd
* @return bool
*/
function insert_new_user($link, $user_name, $passwd){
    $sql = 'INSERT INTO ec_user_table(user_name, password, created_date, updated_date) 
            VALUES(\'' .$user_name. '\', \'' .$passwd. '\', now(), now())';
            
    return edit_db($link, $sql);
}
