<?php
/*****************
* tool用チェック *
******************/
/**
* 商品名入力値チェック
*/
function item_name_check($input_name){
    if(mb_strlen($input_name) === 0){
        $check_result = '商品名を入力してください';
    }else{
        $check_result = true;
    }
    return $check_result;
}

/**
* 値段入力値チェック
*/
function price_check($input_price){
    $regexp = '^([1-9][0-9]*|0)$';
    
    if(preg_match('/' .$regexp. '/', $input_price) !== 1){
        $check_result = '値段は0以上の整数で入力してください';
    }else{
        $check_result = true;
    }
    return $check_result;
}

/**
* 在庫数入力値チェック
*/
function stock_check($input_stock){
    $regexp = '^([1-9][0-9]*|0)$';
    
    if(preg_match('/' .$regexp. '/', $input_stock) !== 1){
        $check_result = '在庫数は0以上の整数で入力してください';
    }else{
        $check_result = true;
    }
    return $check_result;
}

/**
* ステータス入力値チェック
*/
function status_check($input_status){
    if($input_status !== '0' && $input_status !== '1'){
        $check_result = '公開・非公開を選択してください';
    }else{
        $check_result = true;
    }
    return $check_result;
}

/***********************
* user_entry用チェック *
************************/

/**
* ユーザー名、パスワード入力値チェック
*/
function user_and_passwd_check($input_name, $input_passwd){
    $regexp = '[a-zA-z0-9]{6,}';
    
    if(preg_match('/' .$regexp. '/', $input_name) !== 1 
            && preg_match('/[a-zA-z0-9]{6,}/', $input_passwd) !== 1){
        
        $check_result = 'ユーザー名とパスワードは半角英数字6文字以上で入力してください';
    
    }else if(preg_match('/' .$regexp .'/', $input_name) !== 1){
        
        $check_result = 'ユーザー名は半角英数字6文字以上で入力してください';
    
    
    }else if(preg_match('/' .$regexp. '/', $input_passwd) !== 1){
        
        $check_result = 'パスワードは半角英数字6文字以上で入力してください';
        
    }else{
        
        $check_result = true;
    }
    
    return $check_result;
}

/**
* 同一ユーザー名チェック
*/
function same_user_check($link, $user_name){
    $list = array();
    $list = get_user_name($link, $user_name);
    
    if(count($list) !== 0){
        $check_result = '入力のユーザー名は登録済みです';
    }else{
        $check_result = true;
    }
    
    return $check_result;
}

/*****************
* cart用チェック *
******************/
/**
* 数量入力値チェック
*/
function ammount_check($input_ammount){
    $regexp = '^([1-9][0-9]*)$';
    
    if(preg_match('/' .$regexp. '/', $input_ammount) !== 1){
        $check_result = '数量は１以上の整数で入力してください';
    }else{
        $check_result = true;
    }
    return $check_result;
}



/*****************
* result用チェック *
******************/
/**
* 現在のステータスチェック
*/
function now_status_check($list){
    foreach($list as $value){
        if($value['status'] === '0'){
            $check_result = '現在' . $value['name'] . 'は購入できません';
        }else{
            $check_result = true;
        }
        return $check_result;
    }
}
/**
* 現在の在庫チェック
*/
function now_stock_check($list){
    foreach($list as $value){
        if($value['stock'] === '0'){
            $check_result = $value['name'] .'は売り切れです';
        }else if(($value['stock'] - $value['amount']) < 0){
            $check_result = $value['name'] .'の在庫数は残り' .$value['stock'].'です';
        }else{
            $check_result = true;
        }
        return $check_result;
    }
}