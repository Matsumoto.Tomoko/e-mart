<?php
/*******
* tool *
********/

/**
* 商品tableに商品を追加する
* @param obj $link DBハンドル
* @param $new_name, $new_price, $new_image_name, $new_status
* @return bool
*/
function insert_new_item_into_item_table($link, $new_name, $new_price, $new_image_name, $new_status){
    $sql = "INSERT INTO ec_item_table(name, price, img, status, created_date,updated_date) 
            VALUES('$new_name', $new_price, '$new_image_name', $new_status, now(), now())";
    
    return edit_db($link, $sql);
}

/**
* 在庫tableに商品を追加する
* @param obj $link DBハンドル
* @param $item_id, $new_stock
* @return bool
*/
function insert_new_item_ec_stock_table($link, $item_id, $new_stock){
    $sql = "INSERT INTO ec_stock_table(item_id, stock, created_date, updated_date) 
            VALUES($item_id, $new_stock, now(), now())";
    
    return edit_db($link, $sql);
}

/**
* 在庫tableの在庫数を変更する
* @param obj $link DBハンドル
* @param $update_stock, $item_id
* @return bool
*/
function update_stock_number($link, $update_stock, $item_id){
    $sql = 'UPDATE ec_stock_table SET stock = ' .$update_stock. ', updated_date = now() 
    WHERE item_id = ' .$item_id. '';
    
    return edit_db($link, $sql);
}

/**
* 商品tableのステータスを変更する
* @param obj $link DBハンドル
* @param $item_id
* @return bool
*/
function update_status($link, $item_id){
    // 現在のステータスを取得
    $sql = 'SELECT status FROM ec_item_table WHERE item_id = ' .$item_id. '';
    
    $now_status = array();
    $now_status = get_as_array($link, $sql);
    
    if($now_status[0]['status'] === '0'){
        // updateのSQL
        $sql = 'UPDATE ec_item_table SET status = 1, updated_date = now() WHERE item_id = ' .$item_id. '';
    }else if($now_status[0]['status'] === '1'){
        $sql = 'UPDATE ec_item_table SET status = 0, updated_date = now() WHERE item_id = ' .$item_id. '';
    }
    
    return edit_db($link, $sql);
}

/**
* 商品tableから商品を削除する
* @param obj $link DBハンドル
* @param $item_id
* @return bool
*/
function delete_from_ec_item_table($link, $item_id){
    $sql = 'DELETE FROM ec_item_table WHERE item_id = ' .$item_id. '';
    
    return edit_db($link, $sql);
}

/**
* 在庫tableから商品を削除する
* @param obj $link DBハンドル
* @param $item_id
* @return bool
*/
function delete_from_ec_stock_table($link, $item_id){
    $sql = 'DELETE FROM ec_stock_table WHERE item_id = ' .$item_id. '';
    
    return edit_db($link, $sql);
}

/**
* 商品情報リストを取得する
* @param obj $link DBハンドル
* @return array 商品配列データ
*/
function get_item_list_for_manage($link){
    $sql = 'SELECT it.item_id, it.img, it.name, it.price, st.stock, it.status FROM ec_item_table AS it 
            LEFT JOIN ec_stock_table AS st ON st.item_id = it.item_id';
            
    return get_as_array($link, $sql);

}
